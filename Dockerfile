FROM node:7
ARG NPM_REGISTRY
ENV NPM_REGISTRY=${NPM_REGISTRY}
RUN npm config set registry ${NPM_REGISTRY}
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD npm start
EXPOSE 3000
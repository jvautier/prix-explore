.PHONY: serve build clean

# See https://gist.github.com/cobyism/4730490 for github pages 
serve:
	docker-compose up -d
	docker-compose logs -f

build:
	mkdir -p ./docs
	docker-compose run explore npm run build
	touch ./docs/.nojekyll
	sudo chown -R $USER:$USER ./docs

clean:
	rm -rf build/*